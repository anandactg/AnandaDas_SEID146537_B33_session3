floatval<br>
<?php
// Variable Handling Functions -- floatval
$string_with_float="235.269bitm45";

echo floatval($string_with_float);

echo '<br />';
$string_with_interger=2356;
echo floatval($string_with_interger);
echo '<br />';
?>

<br> <br>empty <br>
<?php
// Variable Handling Functions -- empty

$boolemp="";
$boolemp2="Hello";

var_dump(empty($boolemp));
echo '<br />';
var_dump(empty($boolemp2));
echo '<br />';
?>

<br> <br>isArray  <br>
<?php
// Variable Handling Functions -- isArray

$a= array('I ', ' am ', ' in ', ' BITM.');
$b= array(' ', '  ', '  ', ' ');
var_dump(is_array($a));
echo '<br>';
var_dump(is_array($b));
echo '<br />';

?>

<br> <br>isNull <br>
<?php
// Variable Handling Functions -- isNull

$a= array('I ', ' am ', ' in ', ' BITM.');
$b="";
$c=null;
echo 'I have got '.var_dump(is_null($a));
echo '<br>';
var_dump(is_null($b));
echo '<br />';
var_dump(is_null($c));

?>

<br> <br> isset<br>

<?php
// Variable Handling Functions -- isset

$a= '';
$b=20;
$c=null;
echo 'I have got ';
    var_dump(is_null($a));
echo '<br>';
var_dump(is_null($b));
echo '<br />';
var_dump(is_null($c));

?>

<br> <br> print_r<br>

<?php
// Variable Handling Functions -- print_r

$a = array ('a' => 'apple', 'b' => ' banana', 'c' => array ('x', 'y', 'z'));
// $a=1;
print_r ($a);
echo '<br>';
echo $a['a'].$a['b'];
?>

<br> <br> get type<br>

<?php
// Variable Handling Functions -- gettype


$a=1;
$b="Hello";
var_dump(gettype($a));
echo '<br>';
echo $a;
echo '<br>';
var_dump(gettype($b));
echo '<br>';
echo $b;
echo '<br>';
?>

<br> <br> Serialize and unserialize <br>

<?php
// Variable Handling Functions -- serialize and un serialize

$arr = array("My value 1","My value 2");

$str= serialize($arr);

echo $str;

$myPreviousValue= unserialize($str);
echo "<br>";
//var_dump($myPreviousValue);
print_r($myPreviousValue);
?>

<br> <br> is bool <br>

<?php
// Variable Handling Functions -- is bool

$var1 = 1;
$var2 = 0;
$var3=null;
$var4="String";


var_dump(boolval($var1));
echo "<br>";
var_dump(boolval($var2));
echo "<br>";
var_dump(boolval($var3));
echo "<br>";
var_dump(boolval($var4));
echo "<br>";
?>


<br> <br> intval <br>

<?php
// Variable Handling Functions -- intval

$var1 = 1;
$var2 = 0;
$var3=null;
$var4="String";


var_dump(intval($var1));
echo "<br>";
var_dump(intval($var2));
echo "<br>";
var_dump(intval($var3));
echo "<br>";
var_dump(intval($var4));
echo "<br>";
?>

<br> <br> is_scalar <br>

<?php
// Variable Handling Functions -- is_scalar
// Scalar variables are those containing an integer, float, string or boolean. Types array, object and resource are not scalar.
$var1 = 1;
$var2 = 0;
$var3=null;
$var4="String";


var_dump(is_scalar($var1));
echo "<br>";
var_dump(is_scalar($var2));
echo "<br>";
var_dump(is_scalar($var3));
echo "<br>";
var_dump(is_scalar($var4));
echo "<br>";
?>
