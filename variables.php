<?php
// practising Array
$man=array("oldman","youngman","boy");

echo "Different types of man are ".$man[0]." or ".$man[1]." and ".$man[2];
echo '<br />';
echo "We will know ",$man[0]," and ",$man[1];
echo '<br />';

// practising variables
$ami="Ananda";
$tumi="jani na";

echo "$ami $tumi";
echo '<br />';

//$4a will not be valid, because it starts from an integer. Underscore is valid.
$_a=10;
$_4a=20;
// for a interger type variable there no need to echo in quotation
echo $_a." and ".$_4a;

$b=true;
echo $b;

?>